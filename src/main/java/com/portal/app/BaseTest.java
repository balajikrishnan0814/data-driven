package com.portal.app;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;

import com.portal.common.ApplicationUtils;
import com.portal.webpages.HomePage;
import com.portal.webpages.RegisterPage;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

//This class is initializing the webdriver,logfile and pages
@ContextConfiguration(locations = { "/spring-config.xml" })
@Listeners({ListenerTest.class})
public class BaseTest extends AbstractTestNGSpringContextTests {
	public static String TEST_RUN_ID = "43741";
	public static String TESTRAIL_USERNAME = "Balaji.Krishnan@ladbrokes.co.uk";
	public static String TESTRAIL_PASSWORD = "Password@123";
	public static String RAILS_ENGINE_URL = "https://ladbrokescoral.testrail.com/";
	public static final int TEST_CASE_PASSED_STATUS = 1;
	public static final int TEST_CASE_FAILED_STATUS = 5;
	public static WebDriver driver;
	public static ExtentReports extentReports;
	public static ExtentTest extentTest;
	public static Logger logger;
	public static boolean isInitialized = false;
	public static StringBuilder verificationErrors;
	public static String currentDir;
	public int exceptionCount = 0;
	public static JavascriptExecutor js;
	public static String browser;
	public static String driverPath;

	// Initializing the pages
	public ApplicationUtils appUtils = null;
	public HomePage homepage = null;
	public RegisterPage registerpage = null;

	public void initialize(String Browser) throws IOException {
		DesiredCapabilities cap = null;

		if (!isInitialized) {
			// Initalize the logfile

			logger = Logger.getLogger("log4j.properties");
			browser = Browser;
			System.out.println("browser is: " + browser);
			currentDir = System.getProperty("user.dir");

			extentReports = new ExtentReports(currentDir + "/test-output/STMExtentReport.html", true);
			extentReports.addSystemInfo("Host Name", "Mercury").addSystemInfo("Environment", "Automation Testing")
					.addSystemInfo("User Name", "Balaji Krishnan");

			extentReports.loadConfig(new File(currentDir + "\\extent-config.xml"));

			driverPath = currentDir + "\\Drivers\\chromedriver.exe";
			System.setProperty("webdriver.chrome.driver", driverPath);
			if (browser.contains("Chrome")) {
				cap = DesiredCapabilities.chrome();
				cap.setCapability("disable-popup-blocking", true);
				cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				cap.setBrowserName("Chrome");
				driver = new ChromeDriver();
				verificationErrors = new StringBuilder();
				driver.manage().window().maximize();
				js = (JavascriptExecutor) driver;

			}
		}

	}

	public void initializePages() {
		appUtils = new ApplicationUtils();
		homepage = new HomePage();
		registerpage = new RegisterPage();
	}

	// Taking the screeshot
	public void getscreenshot(String screenshotName) {
		// TODO Auto-generated method stub
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		// The below method will save the screen shot in d drive with name
		// "screenshot.png"
		try {
			FileUtils.copyFile(scrFile, new File(currentDir + "\\Screenshot\\" + screenshotName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * Below method will kill driver
	 */
	public void tearDown() {

		if (driver != null) {
			driver.quit();
		}

	}

	@BeforeTest
	public void TestInitialize() throws Exception {

		try {
			// Below two methods called from the Base class
			initialize("Chrome");
			initializePages();
		} catch (Exception e) {

			appUtils.AddErrors(e);
			appUtils.DisplayErrors();
		}
	}

	@AfterMethod
	public void getResult(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {
			extentTest.log(LogStatus.FAIL, "Test Case Failed is " + result.getName());
			extentTest.log(LogStatus.FAIL, "Test Case Failed is " + result.getThrowable());
		} else if (result.getStatus() == ITestResult.SKIP) {
			extentTest.log(LogStatus.SKIP, "Test Case Skipped is " + result.getName());
		}
		// ending test
		// endTest(logger) : It ends the current test and prepares to create HTML report
		extentReports.endTest(extentTest);
	}

	@AfterTest(alwaysRun = true)
	public void afterTestSuite() {

		tearDown();
		extentReports.flush();
		extentReports.close();

	}

	public static void addResultForTestCase(String testCaseId, int status, String error)
			throws IOException, APIException {

		String testRunId = TEST_RUN_ID;

		APIClient client = new APIClient(RAILS_ENGINE_URL);
		client.setUser(TESTRAIL_USERNAME);
		client.setPassword(TESTRAIL_PASSWORD);
		Map data = new HashMap();
		data.put("status_id", status);
		data.put("custom_manual_or_auto", new Integer(2));
		data.put("comment", "Test case passed successfully");
		client.sendPost("add_result_for_case/" + testRunId + "/" + testCaseId + "", data);

	}

	public static String generateUniqueNumber() {
		long epoch = System.currentTimeMillis() / 1000l;
		return String.valueOf(epoch);
	}

}
