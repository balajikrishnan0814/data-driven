package com.portal.webelements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterPageElements {

	@FindBy(name = "firstName")
	public WebElement FIRSTNAME_TEXTBOX;

	@FindBy(name = "lastName")
	public WebElement LASTNAME_TEXTBOX;

	@FindBy(name = "phone")
	public WebElement PHONENUMBER_TEXTBOX;

	@FindBy(name = "userName")
	public WebElement EMAIL_TEXTBOX;

	@FindBy(name = "address1")
	public WebElement ADDRESS1_TEXTBOX;

	@FindBy(name = "address2")
	public WebElement ADDRESS2_TEXTBOX;

	@FindBy(name = "city")
	public WebElement CITY_TEXTBOX;

	@FindBy(name = "state")
	public WebElement STATE_TEXTBOX;

	@FindBy(name = "postalCode")
	public WebElement POSTALCODE_TEXTBOX;

	@FindBy(name = "country")
	public WebElement COUNTRY_DROPDOWN;

	@FindBy(name = "email")
	public WebElement USERNAME_TEXTBOX;

	@FindBy(name = "password")
	public WebElement PASSWORD_TEXTBOX;

	@FindBy(name = "confirmPassword")
	public WebElement CONFIRMPASSWORD_TEXTBOX;
	
	@FindBy(name = "register")
	public WebElement SUBMIT_BUTTON;
	
	@FindBy(xpath = "//b[contains(text(),'user name is')]")
	public WebElement REGISTER_SUCCESSFUL_MSG;
	
	
}
