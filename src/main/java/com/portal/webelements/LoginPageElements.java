package com.portal.webelements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPageElements {
	
	@FindBy(name = "userName")
	public WebElement USERNAME_TEXTBOX;

	@FindBy(name = "password")
	public WebElement PASSWORD_TEXTBOX;

	@FindBy(name = "login")
	public WebElement LOGIN_BUTTON;

}
