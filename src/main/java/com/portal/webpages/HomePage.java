package com.portal.webpages;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.portal.common.ApplicationUtils;
import com.portal.webelements.HomePageElements;
import com.relevantcodes.extentreports.LogStatus;

public class HomePage extends ApplicationUtils {

	public HomePageElements homepagelements;

	public HomePage() {
		homepagelements = new HomePageElements();
		PageFactory.initElements(driver, homepagelements);
	}

	public void openPage(String Url) {

		logger.info("Open mercury website");
		super.goTo(Url);
		Assert.assertTrue(homepagelements.REGISTER_BUTTON.isDisplayed(), "Mercury website not loaded");
		extentTest.log(LogStatus.PASS, "Mercury website is opened");
		logger.info("Mercury website is opened successfully");
	}

	public void clickRegisterButton() {
		logger.info("Click register button from home page");
		Click(homepagelements.REGISTER_BUTTON);
		waitForPageLoaded(driver, 30);
		extentTest.log(LogStatus.PASS, "Register button is clicked");
		logger.info("Register button clicked successfully");
	}

}
