package com.portal.webpages;

import org.openqa.selenium.support.PageFactory;
import com.portal.common.ApplicationUtils;
import com.portal.common.ExcelUtility;
import com.portal.webelements.LoginPageElements;
import com.relevantcodes.extentreports.LogStatus;


public class LoginPage extends ApplicationUtils{
	
public LoginPageElements loginpagelements;
	
	public LoginPage() {
		loginpagelements = new LoginPageElements();
		PageFactory.initElements(driver, loginpagelements);
	}
	
	public void customerLogin() throws Exception {
		logger.info("Check customer login");
		ExcelUtility.setExcelFile(ExcelUtility.Path_TestData + ExcelUtility.File_TestData,"Registered Customer");
		String UserName = ExcelUtility.getCellData(1, 0);
		String Password = ExcelUtility.getCellData(1, 1);
		loginpagelements.USERNAME_TEXTBOX.sendKeys(UserName);
		loginpagelements.PASSWORD_TEXTBOX.sendKeys(Password);
		loginpagelements.LOGIN_BUTTON.click();
		waitForPageLoaded(driver, 30);
		extentTest.log(LogStatus.PASS, "Login is successful,username is :"+UserName+" and password is :"+Password+"");
		extentTest.log(LogStatus.PASS, "Login is successful",extentTest.addScreenCapture(System.getProperty("user.dir")+"\\Screenshot\\"+"LoginSuucessful.png"));
		logger.info("Login is successful");
	}

}
