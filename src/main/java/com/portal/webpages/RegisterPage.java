package com.portal.webpages;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.portal.common.ApplicationUtils;
import com.portal.common.ExcelUtility;
import com.portal.webelements.RegisterPageElements;
import com.relevantcodes.extentreports.LogStatus;

import spring.Player;

public class RegisterPage extends ApplicationUtils  {
	
	public RegisterPageElements registerpagelements;
	
	public RegisterPage() {
		registerpagelements = new RegisterPageElements();
		PageFactory.initElements(driver, registerpagelements);
	}
	
	public String[] fillRegisterForm(Player player) throws Exception {
		logger.info("Fill register form");
		String[] loginDetails = new String[2];
		ExcelUtility.setExcelFile(ExcelUtility.Path_TestData + ExcelUtility.File_TestData,"Registered Customer");
		loginDetails[0]=player.getUsername();
		loginDetails[1]=player.getNewPassword();
		ClearAndEnterText(registerpagelements.FIRSTNAME_TEXTBOX, player.getFirstName());
		ClearAndEnterText(registerpagelements.LASTNAME_TEXTBOX, player.getLastName());
		ClearAndEnterText(registerpagelements.PHONENUMBER_TEXTBOX, player.getPhone());
		ClearAndEnterText(registerpagelements.EMAIL_TEXTBOX, player.getEmail());
		ClearAndEnterText(registerpagelements.ADDRESS1_TEXTBOX, player.getAddress1());
		ClearAndEnterText(registerpagelements.ADDRESS2_TEXTBOX, player.getAddress2());
		ClearAndEnterText(registerpagelements.CITY_TEXTBOX, player.getCity());
		ClearAndEnterText(registerpagelements.STATE_TEXTBOX, player.getState());
		ClearAndEnterText(registerpagelements.POSTALCODE_TEXTBOX, player.getPostalcode());
		registerpagelements.COUNTRY_DROPDOWN.click();
		Thread.sleep(1500);
		registerpagelements.COUNTRY_DROPDOWN.sendKeys(player.getCountry());
		waitForPageLoaded(driver, 30);
		ClearAndEnterText(registerpagelements.USERNAME_TEXTBOX, player.getUsername());
		ClearAndEnterText(registerpagelements.PASSWORD_TEXTBOX, player.getNewPassword());
		ClearAndEnterText(registerpagelements.CONFIRMPASSWORD_TEXTBOX, player.getConfirmPassword());
		registerpagelements.SUBMIT_BUTTON.click();
		waitForPageLoaded(driver, 50);
		Assert.assertTrue(registerpagelements.REGISTER_SUCCESSFUL_MSG.isDisplayed(), "Registration is not successful");
		extentTest.log(LogStatus.PASS, "Registeration is successful,username is :"+player.getUsername()+" and password is :"+player.getNewPassword()+"");
		logger.info("Registration form is filled");
		ExcelUtility.setCellData(loginDetails[0], 1, 0);
		ExcelUtility.setCellData(loginDetails[1], 1, 1);
		ExcelUtility.setCellData("Pass", 1, 2);
		return loginDetails;
	}

}
