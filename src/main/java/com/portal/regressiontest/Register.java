package com.portal.regressiontest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.portal.app.BaseTest;
import com.portal.app.RetryAnalyzerImpl;
import com.portal.common.ApplicationConstants;
import com.portal.webpages.HomePage;
import com.portal.webpages.RegisterPage;

import spring.Player;

public class Register extends BaseTest{

	@Autowired
	@Qualifier("NewPlayer")
	private Player newPlayer;

	@Test(retryAnalyzer = RetryAnalyzerImpl.class)
	public void verify_MercuryWebSite_Registration() {

		try {
			extentTest = extentReports.startTest("Check register button is clicked from mercury website");
			// open mercury website
			HomePage homePage = new HomePage();
			homePage.openPage(ApplicationConstants.MercuryURL);

			// click register button
			homePage.clickRegisterButton();

			// setting username
			newPlayer.setUsername("user" + BaseTest.generateUniqueNumber());

			// Fill register form
			RegisterPage registerPage = new RegisterPage();

			registerPage.fillRegisterForm(newPlayer);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail("Registration is not successful");
		}

	}

}
