package com.portal.regressiontest;

import java.io.IOException;

import org.testng.annotations.Test;

import com.portal.app.APIException;
import com.portal.app.BaseTest;
import com.portal.common.ApplicationConstants;
import com.portal.webpages.HomePage;

public class OpenMercurySite extends BaseTest {

	@Test(description = "Verify mercury website is opened=C1032796")
	public void verify_MercuryWebSite() throws IOException, APIException {
		try {
			logger.info("-----------------------Start mercury home page test---------------------");
			extentTest = BaseTest.extentReports.startTest("Check mercury website is opened");
			// open mercury website
			HomePage homePage = new HomePage();
			homePage.openPage(ApplicationConstants.MercuryURL);
			BaseTest.addResultForTestCase("1032796", TEST_CASE_PASSED_STATUS, "");
		} catch (IOException e) {
			BaseTest.addResultForTestCase("1032796", TEST_CASE_FAILED_STATUS, "");

		} catch (APIException e) {
			BaseTest.addResultForTestCase("1032796", TEST_CASE_FAILED_STATUS, "");

		}
		logger.info("-----------------------End mercury home page test---------------------");

	}

}
