package com.portal.regressiontest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.portal.app.BaseTest;
import com.portal.app.RetryAnalyzerImpl;
import com.portal.common.ApplicationConstants;
import com.portal.webpages.HomePage;
import com.portal.webpages.LoginPage;

import spring.Player;

public class CustomerLogin extends BaseTest  {

	@Autowired
	@Qualifier("NewPlayer")
	private Player newPlayer;

	@Test(retryAnalyzer = RetryAnalyzerImpl.class)
	public void verify_MercuryWebSite_Login()  {

		try {
			
			extentTest = extentReports.startTest("Check login functionality from mercury website");
			// open mercury website
			HomePage homePage = new HomePage();
			homePage.openPage(ApplicationConstants.MercuryURL);

			LoginPage loginPage = new LoginPage();
			loginPage.customerLogin();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail("Login is not successful");
		}

	}

}
