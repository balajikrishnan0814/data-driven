package com.portal.common;

import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.portal.app.BaseTest;

public class ApplicationUtils extends BaseTest {
	
	

	/**
	 * Scroll page to see given element
	 *
	 * @param intoView
	 *            true - the top of the element will be aligned to the top of the
	 *            visible area of the scrollable ancestor false - the bottom of the
	 *            element will be aligned to the bottom of the visible area of the
	 *            scrollable ancestor
	 * @param pixels
	 *            for vertical scroll
	 * @param element
	 * @throws InterruptedException
	 */
	public void scrollToObject(boolean intoView, int pixels, WebElement element) {

		try {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView(" + intoView + ");", element);
			jse.executeScript("window.scrollBy(0, " + pixels + ");", "");
			Thread.sleep(500);
		} catch (Exception e) {
			exceptionCount++;
			verificationErrors.append("Error while scrolling to object" + e);
		} // after scroll we need to wait for scroll animation before click
	}

	public void scrollToObjectTop(WebElement element) {

		try {
			scrollToObject(true, -150, element);
		} catch (Exception e) {
			exceptionCount++;
			verificationErrors.append("Error while scrolling to object top" + e);
		}
	}

	public void scrollToObjectBottom(WebElement element) {

		try {
			scrollToObject(false, 150, element);
		} catch (Exception e) {

			exceptionCount++;
			verificationErrors.append("Error while scrolling to object bottom" + e);
		}
	}

	public void goTo(String url) {
		try {
		driver.get(url);
		waitForPageLoaded(driver, 60);
		}
		catch (Exception e) {

			exceptionCount++;
			verificationErrors.append(url + "is not opened" + e);
		}
	}

	protected void waitForPageLoaded(WebDriver driver, int timeout) {
		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};

		Wait<WebDriver> wait = new WebDriverWait(driver, timeout);
		try {
			wait.until(expectation);
		} catch (Exception e) {

			exceptionCount++;
			verificationErrors.append("Error while loading the page" + e);
		}
	}

	public void ClearAndEnterText(WebElement element, String inputText) {
		try {
			element.clear();
			element.sendKeys(inputText);
		} catch (Exception e) {
			exceptionCount++;
			verificationErrors.append("Error while Clearing and Entering the text in the text box" + e);
		}
	}

	public WebElement WaitForElement(WebElement Element, int timeOut) {
		WebElement element = null;
		try {
			WebDriverWait wait = new WebDriverWait(driver, timeOut);
			element = wait.until(ExpectedConditions.visibilityOf(Element));

		} catch (Exception e) {
			exceptionCount++;
			verificationErrors.append(" DismissAlert| Error while verifying the alert" + e);
		}
		return element;
	}

	public void EnterText(WebElement element, String inputText) {
		try {
			element.sendKeys(inputText);
		} catch (Exception e) {
			exceptionCount++;
			verificationErrors.append("Error while Entering the text in the text box" + e);
		}
	}

	/**
	 * Select option (by visible text) from dropdown list
	 */
	public void selectFromDropDownList(WebElement list, String option) {
		try {
			if (option != null) {

				new Select(list).selectByVisibleText(option);
			}
		} catch (Exception e) {

			exceptionCount++;
			verificationErrors.append(option + "is not selected from dropdownlist");
		}
	}

	/**
	 * Select option (by value) from dropdown list
	 */
	protected void selectByValueFromDropDownList(WebElement list, String option) {
		try {
			if (option != null) {

				scrollToObjectTop(list);
				new Select(list).selectByValue(option);
			}
		} catch (Exception e) {

			exceptionCount++;
			verificationErrors.append(option + "is not selected from dropdownlist");
		}
	}

	public boolean isElementDisplayed(WebElement Element) {

		try {

			if (Element.isDisplayed()) {

				return true;

			} else {

				return false;
			}

		} catch (Exception e) {

			exceptionCount++;
			verificationErrors.append(Element + "is not displayed");
		}
		return false;
	}

	public boolean isEnabled(WebElement Element) {
		try {

			if (Element.isEnabled()) {

				return true;

			} else {

				return false;
			}

		} catch (Exception e) {

			exceptionCount++;
			verificationErrors.append(Element + "is not enabled");
		}
		return false;
	}

	public void Click(WebElement Element) {
		try {

			Element.click();

		} catch (Exception e) {

			exceptionCount++;
			verificationErrors.append(Element + "is not clicked");
		}

	}

	public void AcceptAlert() {
		try {
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (Exception e) {
			exceptionCount++;
			verificationErrors.append(" AcceptAlert| Error while verifying the alert" + e);
		}
	}

	public void DismissAlert() {
		try {
			Alert alert = driver.switchTo().alert();
			alert.dismiss();
		} catch (Exception e) {
			exceptionCount++;
			verificationErrors.append(" DismissAlert| Error while verifying the alert" + e);
		}
	}

	public String switchToNewestWindow() {

		String oldHandle = driver.getWindowHandle();

		for (String winHandle : driver.getWindowHandles()) { // go through all windows, the last is the newest
			driver.switchTo().window(winHandle);

		}

		return oldHandle;
	}

	public void SwitchtoFrame(String windowName) {
		driver.switchTo().frame(windowName);

	}

	public void DisplayErrors() throws Exception {
		try {
			if (exceptionCount > 0) {
				throw new Exception(verificationErrors.toString());
			}
		} finally {
			exceptionCount = 0;
			verificationErrors.delete(0, exceptionCount);
		}
	}

	public void AddErrors(Exception e) {
		logger.error("Error: " + e);
		exceptionCount++;
		verificationErrors.append(e);
	}
}
